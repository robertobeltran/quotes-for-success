var currentQuote = '', currentAuthor = '';
var authors = {
  "Benjamin Franklin": {
    picture: "https://upload.wikimedia.org/wikipedia/commons/2/25/Benjamin_Franklin_by_Joseph_Duplessis_1778.jpg",
    about: "Benjamin Franklin was an American polymath and one of the Founding Fathers of the United States."
  },
  "Warren E. Buffet": {
    picture: "https://upload.wikimedia.org/wikipedia/commons/5/51/Warren_Buffett_KU_Visit.jpg",
    about: "Warren Edward Buffett is an American business magnate and investor. As of February 12, 2019 he is the third-wealthiest person in the world."
  }
}
function getQuote() {
  var quotes = [
    {
      quote: "Price is what you pay. Value is what you get.",
      author: "Warren E. Buffet"
    },
    {
      quote: "Rule No.1: Never lose money. Rule No.2: Never forget rule No.1.",
      author: "Warren E. Buffet"
    },
    {
      quote: "We simply attempt to be fearful when others are greedy and to be greedy only when others are fearful.",
      author: "Warren E. Buffet"
    },
    {
      quote: "Someone's sitting in the shade today because someone planted a tree a long time ago.",
      author: "Warren E. Buffet"
    },
    {
      quote: "You may delay, but time will not.",
      author: "Benjamin Franklin"
    },
    {
      quote: "By failing to prepare, you are preparing to fail.",
      author: "Benjamin Franklin"
    }
  ];
  var index = Math.floor(Math.random() * (quotes.length)); // calculate a random index in the quotes array
  console.log(index);
  return quotes[index];
}
function newQuote() {
    var quote;
    do {
      quote = getQuote();
    } while (quote["quote"] == currentQuote);
    currentQuote = quote["quote"];
    currentAuthor = quote["author"]
    $("#text").text(currentQuote);
    $("#author").text(currentAuthor);
    $("#author-image").attr("src", authors[currentAuthor]["picture"]);
    $("#about-author").text(authors[currentAuthor]["about"]);
}

$(document).ready(function() {
  $("#new-quote").on("click", newQuote);
  newQuote();
});
